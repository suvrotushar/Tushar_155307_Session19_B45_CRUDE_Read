<?php

namespace App\Hobbies;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Hobbies extends DB
{
    private $id;
    private $name;
    private $hobbie1;
    private $hobbie2;

    public function setData($allPostData){
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("hobby1",$allPostData)){
            $this->hobbie1 = $allPostData['hobby1'];
        }
        if(array_key_exists("hobby2",$allPostData)){
            $this->hobbie2 = $allPostData['hobby2'];
        }
    }
    public function store(){
        $arraData = array($this->name, $this->hobbie1,$this->hobbie2);
        $query = "insert into hobbies (name, hobby1, hobby2) VALUES (?,?,?)";
        $sth = $this->DBH->prepare($query);
        $result = $sth->execute($arraData);
        if($result){
            Message::setMessage("Success! Data has been stored Successfully...");
        }else{
            Message::setMessage("Failed! Data has not been stored Successfully...");
        }
        Utility::redirect("create.php");
    }
    public function view(){

        $sql = "Select * from hobbies where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function index(){

        $sql = "Select * from hobbies where soft_deleted='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function trashed(){

        $sql = "Select * from hobbies where soft_deleted='Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

}