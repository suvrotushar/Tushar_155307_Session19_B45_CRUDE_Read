<?php
/**
 * Created by PhpStorm.
 * User: p1r4t3
 * Date: 1/26/2017
 * Time: 3:16 PM
 */

namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
class SummaryOfOrganization extends DB
{
    private $id;
    private $name;
    private $nameorg;
    private $txtarea;

    public function setData($allPostData)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if (array_key_exists("name", $allPostData)) {
            $this->name = $allPostData['name'];
        }
        if (array_key_exists("nameorg", $allPostData)) {
            $this->nameorg = $allPostData['nameorg'];
        }
        if (array_key_exists("txtarea", $allPostData)) {
            $this->txtarea = $allPostData['txtarea'];
        }
    }

    public function store()
    {
        $arraData = array($this->name, $this->nameorg, $this->txtarea);
        $query = "insert into summary_of_organization (name, nameorg, txtarea) VALUES (?,?,?)";
        $sth = $this->DBH->prepare($query);
        $result = $sth->execute($arraData);
        if ($result) {
            Message::setMessage("Success! Data has been stored Successfully...");
        } else {
            Message::setMessage("Failed! Data has not been stored Successfully...");
        }
        Utility::redirect("create.php");
    }
    public function view(){

        $sql = "Select * from summary_of_organization where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function index(){

        $sql = "Select * from summary_of_organization where soft_deleted='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function trashed(){

        $sql = "Select * from summary_of_organization where soft_deleted='Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

}


