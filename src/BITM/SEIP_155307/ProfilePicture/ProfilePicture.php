<?php

namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class ProfilePicture extends DB
{
    Private $name;
    private $tmp_name;
    private $pic_name;

    public function setData($allFileData, $allPostData)
    {

        if (array_key_exists("name", $allPostData)) {
            $this->name = $allPostData['name'];
        }
        if (array_key_exists("picture", $allFileData)) {
            $this->tmp_name = $allFileData['picture']['tmp_name'];
            $this->pic_name = $allFileData['picture']['name'];
        }
    }
    public function store(){
        $arraData = array($this->name, $this->pic_name, $this->tmp_name);
        $query = "insert into profile_pic (name, pic_name, tmp_name) VALUES (?,?,?)";
        $sth = $this->DBH->prepare($query);
        $result = $sth->execute($arraData);
        if ($result) {
            Message::setMessage("Success! Data has been stored Successfully...");
        } else {
            Message::setMessage("Failed! Data has not been stored Successfully...");
        }
        Utility::redirect("create.php");
    }
}