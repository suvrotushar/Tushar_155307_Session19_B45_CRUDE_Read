<?php

namespace App\City;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class City extends DB
{
    private $id;
    private $name;
    private $city_name;
    private $country_name;

    public function setData($allPostData){
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("city",$allPostData)){
            $this->city_name = $allPostData['city'];
        }
        if(array_key_exists("country",$allPostData)){
            $this->country_name = $allPostData['country'];
        }
    }
    public function store(){
        $arraData = array($this->name, $this->city_name, $this->country_name);
        $query = "insert into city(name, city_name, country_name) VALUES (?,?,?)";
        $sth = $this->DBH->prepare($query);
        $result = $sth->execute($arraData);
        if($result){
            Message::setMessage("Success! Data has been stored Successfully...");
        }else{
            Message::setMessage("Failed! Data has not been stored Successfully...");
        }
        Utility::redirect("create.php");
    }
    public function view(){

        $sql = "Select * from city where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function index(){

        $sql = "Select * from city where soft_deleted='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function trashed(){

        $sql = "Select * from city where soft_deleted='Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}