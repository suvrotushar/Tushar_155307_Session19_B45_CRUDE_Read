<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies</title>
    <link rel="stylesheet" href="../../../resource/css/style.css">
    <script src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <script>
        jQuery(function($){
            $('.message').fadeIn(500);
            $('.message').fadeOut(1500);
            $('.message').fadeIn(500);
            $('.message').fadeOut(1500);
            $('.message').fadeIn(500);
            $('.message').fadeOut(1500);
        });
    </script>
</head>
<body>
<div id="InputForm">
    <form action="store.php" method="post">
        <label>Enter Your Name &nbsp;&nbsp;&nbsp;&nbsp; : </label>
        <input type="text" name="name" placeholder="  your name" required><br><br>
        <label>Enter Your First Hobby : </label>
        <input type="text" name="hobby1" placeholder="Let us know your hobby" required><br><br>
        <label>Enter Your Second Hobby : </label>
        <input type="text" name="hobby2" placeholder="Let us know your hobby" ><br><br>
        <input type="submit">
    </form>
</div>
<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
$msg = Message::getMessage();

if($msg=="Success! Data has been stored Successfully..."){
    echo "<div id='success' class='message'><h3>$msg</h3></div>";
}elseif($msg=="Failed! Data has not been stored Successfully..."){
    echo "<div id='error' class='message'><h3>$msg</h3></div>";
}
?>
</body>
</html>