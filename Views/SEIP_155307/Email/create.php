<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email</title>
    <link rel="stylesheet" href="../../../resource/css/style.css">
    <script src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <script>
        jQuery(function($){
            $('.message').fadeIn(500);
            $('.message').fadeOut(1500);
            $('.message').fadeIn(500);
            $('.message').fadeOut(1500);
            $('.message').fadeIn(500);
            $('.message').fadeOut(1500);
        });
    </script>
</head>
<body>
    <div id="InputForm">
        <form action="store.php" method="post">
            <label>Enter Your Name &nbsp;: </label>
            <input type="text" name="name" placeholder="  your name" required><br><br>
            <label>Enter Your Email : </label>
            <input type="email" name="email" placeholder="  your email" required><br><br>
            <input type="submit">
        </form>
    </div>
    <?php
        require_once("../../../vendor/autoload.php");
        use App\Message\Message;
        $msg = Message::getMessage();

        if($msg=="Success! Data has been stored Successfully..."){
            echo "<div id='success' class='message'><h3>$msg</h3></div>";
        }elseif($msg=="Failed! Data has not been stored Successfully..."){
            echo "<div id='error' class='message'><h3>$msg</h3></div>";
        }
    ?>
</body>
</html>