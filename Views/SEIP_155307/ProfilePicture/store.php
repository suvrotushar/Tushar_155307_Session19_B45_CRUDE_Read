<?php
require_once ("../../../vendor/autoload.php");
use App\Message;

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$objProfilePicture->setData($_FILES,$_POST);
$objProfilePicture->store();